﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Usergrid.Sdk.Model;

namespace PremierCIODataLoading.Entities
{
    class AgendaSponsorRelation : UsergridEntity
    {
        public string agendaitemname { get; set; }
        public string itemtype { get; set; }
        public string sidebartext { get; set; }
        public string sponsorname { get; set; }
    }
}
