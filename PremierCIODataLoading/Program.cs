﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Usergrid.Sdk;
using PremierCIODataLoading.Entities;
using System.IO;
using Microsoft.Office.Interop.Excel;
using System.Data.OleDb;
using System.Data;
using Usergrid.Sdk.Model;
using System.Net;
using System.Globalization;
using System.Collections;

namespace PremierCIODataLoading
{
    class Program
    {
        static void Main(string[] args)
        {
            //Fetch org-id,app-id and app-uri from app.config
            string OrgID = ConfigurationSettings.AppSettings["ORGANIZATIONID"];
            string appID = ConfigurationSettings.AppSettings["APPLICATIONID"];
            string appURI = ConfigurationSettings.AppSettings["AppURI"];
            string clientID = ConfigurationSettings.AppSettings["ClientID"];
            string clientSecret = ConfigurationSettings.AppSettings["ClientSecret"];
            string AttendeeFilePath = ConfigurationSettings.AppSettings["AttendeeFilePath"];
            string ConferencesFilePath = ConfigurationSettings.AppSettings["ConferencesFilePath"];
            string SponsorsFilePath = ConfigurationSettings.AppSettings["SponsorsFilePath"];
            string SpeakersFilePath = ConfigurationSettings.AppSettings["SpeakersFilePath"];
            
            Program pr = new Program();
            
            //Fetching data from excel for conferences and pushing into dataset.
            
            DataSet ds = pr.conferences();
            System.Data.DataTable dt = ds.Tables[0];

            //Initialize UserGrid client.
            Client clnt = new Client(OrgID, appID, appURI);
            clnt.Login(clientID, clientSecret, AuthType.Organization);
            //Conference c = clnt.GetEntity<Conference>("Conferences", "EDINA MN");
            
            //GetEntities gt = new GetEntities(clnt);
            //gt.getConferences();
            /*DeleteEntities delObj = new DeleteEntities(clnt);
            delObj.deleteAllEntities("conferences");
            delObj.deleteAllEntities("sponsors");
            delObj.deleteAllEntities("users");
            delObj.deleteAllEntities("speakers");
            delObj.deleteAllEntities("agendaitems");
            delObj.deleteAllEntities("maps");*/
            UsergridCollection<Conference> conf = new UsergridCollection<Conference>();
            foreach (DataRow dr in dt.Rows)
            {
                Conference confrnc = new Conference();
                
                confrnc = createConferenceObject(dr);
                Conference response = clnt.CreateEntity<Conference>("Conferences",confrnc);
                
                DataSet dsSpon = new DataSet();
                dsSpon = pr.sponsors(confrnc.conferencename);//Get the Sponsors for this conference.
                foreach (DataRow drSpon in dsSpon.Tables[0].Rows)
                {
                    Sponsor sp = new Sponsor();
                    sp = createSponsorObject(drSpon);
                    Sponsor spresponse = clnt.CreateEntity<Sponsor>("Sponsors",sp);

                    //Sponsors with Conferences
                    Connection connect = new Connection();
                    connect.ConnecteeCollectionName = "sponsors";
                    connect.ConnecteeIdentifier = spresponse.Uuid;
                    connect.ConnectionName = "sponsorfor";
                    connect.ConnectorCollectionName = "Conferences";
                    connect.ConnectorIdentifier = response.Uuid;
                    clnt.CreateConnection(connect);

                    //Conferences with sponsors
                    Connection connectconf = new Connection();
                    connectconf.ConnecteeCollectionName = "Conferences";
                    connectconf.ConnecteeIdentifier = response.Uuid;
                    connectconf.ConnectionName = "sponsoredby";
                    connectconf.ConnectorCollectionName = "sponsors";
                    connectconf.ConnectorIdentifier = spresponse.Uuid;
                    clnt.CreateConnection(connectconf);
                }
                if(confrnc.conferencename == "NEW BRUNSWICK 2015")
                {
                    DataSet dsUser = new DataSet();
                    dsUser = pr.attendee();
                    //Create UserGridUser Entity.
                    foreach (DataRow drUser in dsUser.Tables[0].Rows)
                    {
                        UsergridUser user = new UsergridUser();
                        user = createUserObject(drUser);
                        try
                        {
                            UsergridUser usrresponse = clnt.CreateEntity<UsergridUser>("users", user);

                            //users with conferences
                            Connection connectattendee = new Connection();
                            connectattendee.ConnecteeCollectionName = "users";
                            connectattendee.ConnecteeIdentifier = usrresponse.Uuid;
                            connectattendee.ConnectionName = "registersfor";
                            connectattendee.ConnectorCollectionName = "Conferences";
                            connectattendee.ConnectorIdentifier = response.Uuid;
                            clnt.CreateConnection(connectattendee);

                            //conferences with users
                            Connection connectconfattendee = new Connection();
                            connectconfattendee.ConnecteeCollectionName = "Conferences";
                            connectconfattendee.ConnecteeIdentifier = response.Uuid;
                            connectconfattendee.ConnectionName = "registeredfor";
                            connectconfattendee.ConnectorCollectionName = "users";
                            connectconfattendee.ConnectorIdentifier = usrresponse.Uuid;
                            clnt.CreateConnection(connectconfattendee);
                        }
                        catch(Exception ex)
                        {
                            string message = ex.Message;
                            string FILE_PATH = ConfigurationSettings.AppSettings["LOG_FILE_PATH"] + "LogTextFile.log";
                            
                            FileStream fs = null;
                            if (!File.Exists(FILE_PATH))
                            {
                                using (fs = File.Create(FILE_PATH))
                                {
                                }
                            }
                            using (FileStream file = new FileStream(FILE_PATH, FileMode.Append, FileAccess.Write))
                            {
                                StreamWriter streamWriter = new StreamWriter(file);
                                streamWriter.WriteLine("\n\n\n");
                                streamWriter.WriteLine("Error: " + System.DateTime.Now + " ----> " + message + "\n\n");
                                Console.WriteLine("Error: " + System.DateTime.Now + " ----> " + message + "\n\n");
                                streamWriter.Close();
                            }
                            continue;
                        }
                        
                    }

                    //create Maps Colection.
                    Maps map = createMapObject();
                    Maps mapresponse = clnt.CreateEntity<Maps>("maps", map);

                    //conferences "hasmaps" maps
                    Connection connectconfmap = new Connection();
                    connectconfmap.ConnecteeCollectionName = "Conferences";
                    connectconfmap.ConnecteeIdentifier = response.Uuid;
                    connectconfmap.ConnectionName = "hasmaps";
                    connectconfmap.ConnectorCollectionName = "maps";
                    connectconfmap.ConnectorIdentifier = mapresponse.Uuid;
                    clnt.CreateConnection(connectconfmap);

                    //create Speakers Collection.
                    DataSet dsSpeakers = pr.speakers();
                    foreach (DataRow drspeak in dsSpeakers.Tables[0].Rows)
                    {
                        Speakers speak = new Speakers();
                        speak = createSpeakerObject(drspeak);
                        Speakers resSpeak = clnt.CreateEntity<Speakers>("Speakers", speak);
                    }


                    DataSet dsetagenda = new DataSet();
                    dsetagenda = pr.agendaItems();
                    AgendaItems parentAgenda = new AgendaItems();
                    foreach(DataRow dragenda in dsetagenda.Tables[0].Rows)
                    {
                        AgendaItems agItems = createAgendaItemObject(dragenda);
                        AgendaItems agresponse = clnt.CreateEntity<AgendaItems>("agendaitems", agItems);
                        
                        if (agresponse.isgroup)
                            parentAgenda = agresponse;
                        else
                        {
                            Connection connparentChildagenda = new Connection();
                            connparentChildagenda.ConnecteeCollectionName = "agendaitems";
                            connparentChildagenda.ConnecteeIdentifier = parentAgenda.Uuid;
                            connparentChildagenda.ConnectionName = "contains";
                            connparentChildagenda.ConnectorCollectionName = "agendaitems";
                            connparentChildagenda.ConnectorIdentifier = agresponse.Uuid;
                            clnt.CreateConnection(connparentChildagenda);

                            Connection connchildParentagenda = new Connection();
                            connchildParentagenda.ConnecteeCollectionName = "agendaitems";
                            connchildParentagenda.ConnecteeIdentifier = agresponse.Uuid;
                            connchildParentagenda.ConnectionName = "containedin";
                            connchildParentagenda.ConnectorCollectionName = "agendaitems";
                            connchildParentagenda.ConnectorIdentifier = parentAgenda.Uuid;
                            clnt.CreateConnection(connchildParentagenda);
                        }

                        Connection connectconfAgenda = new Connection();
                        connectconfAgenda.ConnecteeCollectionName = "Conferences";
                        connectconfAgenda.ConnecteeIdentifier = response.Uuid;
                        connectconfAgenda.ConnectionName = "hasagendaitems";
                        connectconfAgenda.ConnectorCollectionName = "agendaitems";
                        connectconfAgenda.ConnectorIdentifier = agresponse.Uuid;
                        clnt.CreateConnection(connectconfAgenda);
                    }//end of agenda
                    
                }//end of if 
                
            }//end of first foreach loop

            DataSet dsAgendaSpeaker = new DataSet();
            dsAgendaSpeaker = pr.AgendaSpeaker();
            UsergridCollection<AgendaItems> agendaItemsRel = clnt.GetEntities<AgendaItems>("agendaitems", 1000, null);
            foreach (DataRow dragsp in dsAgendaSpeaker.Tables[0].Rows)
            {
                string speakerRelation = string.Empty;
                string agendaRelation = string.Empty;
                AgendaItemSpeaker aiSpeak = new AgendaItemSpeaker();
                aiSpeak = createAgendaSpeakerObject(dragsp);
                if (aiSpeak.speakertype == "Speaker")
                {
                    speakerRelation = "hasspeakers";
                    agendaRelation = "speakerfor";
                }
                else if (aiSpeak.speakertype == "Moderator")
                {
                    speakerRelation = "hasmoderators";
                    agendaRelation = "moderatorfor";
                }
                else if (aiSpeak.speakertype == "Panelist")
                {
                    speakerRelation = "haspanelists";
                    agendaRelation = "panelistfor";
                }
                else
                    continue;

                Speakers speakRel =  clnt.GetEntity<Speakers>("speakers", aiSpeak.speakername);
                
                //AgendaItems agendaItemsRel = clnt.GetEntity<AgendaItems>("agendaitems", aiSpeak.agendaname);
                foreach (AgendaItems singleagenda in agendaItemsRel)
                {
                    if (singleagenda.agendaitemname == aiSpeak.agendaname)
                    {
                        Connection conAgendaSpeaker = new Connection();
                        conAgendaSpeaker.ConnecteeCollectionName = "agendaitems";
                        conAgendaSpeaker.ConnecteeIdentifier = singleagenda.Uuid;
                        conAgendaSpeaker.ConnectionName = speakerRelation;
                        conAgendaSpeaker.ConnectorCollectionName = "speakers";
                        conAgendaSpeaker.ConnectorIdentifier = speakRel.Uuid;
                        clnt.CreateConnection(conAgendaSpeaker);

                        Connection conSpeakerAgenda = new Connection();
                        conSpeakerAgenda.ConnecteeCollectionName = "speakers";
                        conSpeakerAgenda.ConnecteeIdentifier = speakRel.Uuid;
                        conSpeakerAgenda.ConnectionName = agendaRelation;
                        conSpeakerAgenda.ConnectorCollectionName = "agendaitems";
                        conSpeakerAgenda.ConnectorIdentifier = singleagenda.Uuid;
                        clnt.CreateConnection(conSpeakerAgenda);
                        break;
                    }
                }
                
            }

            /*DataSet dsAgendaSponsor = new DataSet();
            dsAgendaSponsor = pr.AgendaSponsor();
            foreach (DataRow das in dsAgendaSponsor.Tables[0].Rows)
            {
                AgendaSponsorRelation asr = new AgendaSponsorRelation();
                asr = createAgendaSponsorObject(das);
                Sponsor spnsr = clnt.GetEntity<Sponsor>("sponsors", asr.sponsorname);
                foreach (AgendaItems aitms in agendaItemsRel)
                {
                    if ((aitms.agendaitemname == asr.agendaitemname) && (aitms.itemtype == asr.itemtype) && (aitms.sidebartext == asr.sidebartext))
                    {
                        Connection connagendaSponsor = new Connection();
                        connagendaSponsor.ConnecteeCollectionName = "agendaitem";
                        connagendaSponsor.ConnecteeIdentifier = aitms.Uuid;
                        connagendaSponsor.ConnectionName = "sponsoredby";
                        connagendaSponsor.ConnectorCollectionName = "sponsors";
                        connagendaSponsor.ConnectorIdentifier = spnsr.Uuid;
                        clnt.CreateConnection(connagendaSponsor);
                    }
                }
            }*/
        }


        //private DataSet TimeZone()
        //{
        //    DataSet ds = new DataSet();
        //    string sqlquerytimeDiff = "Select * From [Sheet1$A1:B2]";
        //    string constringtimeDiff = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=D:\VisualStudio2013\Projects\PremierCIODataLoading\PremierCIODataLoading\Assets\TimeDiff.xlsx" + ";Extended Properties=\"Excel 12.0;HDR=YES;\"";
        //    OleDbConnection con = new OleDbConnection(constringtimeDiff + "");
        //    OleDbDataAdapter da = new OleDbDataAdapter(sqlquerytimeDiff, con);
        //    da.Fill(ds);
        //    return ds;
        //}


        private static AgendaSponsorRelation createAgendaSponsorObject(DataRow dr)
        {
            AgendaSponsorRelation asrObj = new AgendaSponsorRelation();
            asrObj.agendaitemname = dr["agendaitemname"].ToString();
            asrObj.itemtype = dr["itemtype"].ToString();
            asrObj.sidebartext = dr["sidebartext"].ToString();
            asrObj.sponsorname = dr["sponsorname"].ToString();
            return asrObj;
        }

        private DataSet AgendaSponsor()
        {
            DataSet ds = new DataSet();
            string sqlqueryagendasponsor = "Select * From [NewBrunswick2015$A1:D3]";
            string constringagendasponsor = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=D:\VisualStudio2013\Projects\PremierCIODataLoading\PremierCIODataLoading\Assets\AgendaSponsorsRelation.xlsx" + ";Extended Properties=\"Excel 12.0;HDR=YES;\"";
            OleDbConnection con = new OleDbConnection(constringagendasponsor + "");
            OleDbDataAdapter da = new OleDbDataAdapter(sqlqueryagendasponsor, con);
            da.Fill(ds);
            return ds;
        }

        private static AgendaItemSpeaker createAgendaSpeakerObject(DataRow dragsp)
        {
            AgendaItemSpeaker aitSpeak = new AgendaItemSpeaker();
            aitSpeak.agendaname = dragsp["agendaname"].ToString();
            aitSpeak.speakername = dragsp["speakername"].ToString();
            aitSpeak.speakerfirstname = dragsp["speakerfirstname"].ToString();
            aitSpeak.speakerlastname = dragsp["speakerlastname"].ToString();
            aitSpeak.speakertype = dragsp["speakertype"].ToString();
            return aitSpeak;
        }

        private DataSet AgendaSpeaker()
        {
            DataSet ds = new DataSet();
            string sqlqueryagendaspeaker = "Select * From [AgendaSpeaker$A1:E34]";
            string constringagendaspeaker = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=D:\VisualStudio2013\Projects\PremierCIODataLoading\PremierCIODataLoading\Assets\AgendaSpeakerRelationship.xlsx" + ";Extended Properties=\"Excel 12.0;HDR=YES;\"";
            OleDbConnection con = new OleDbConnection(constringagendaspeaker + "");
            OleDbDataAdapter da = new OleDbDataAdapter(sqlqueryagendaspeaker, con);
            da.Fill(ds);
            return ds;
        }

        public DataSet agendaItems()
        {
            DataSet ds = new DataSet();
            string sqlqueryagenda = "Select * From [NewBrunswick2015$A1:I28]";
            string constringagenda = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=D:\VisualStudio2013\Projects\PremierCIODataLoading\PremierCIODataLoading\Assets\AgendaItems.xlsx" + ";Extended Properties=\"Excel 12.0;HDR=YES;\"";
            OleDbConnection con = new OleDbConnection(constringagenda + "");
            OleDbDataAdapter da = new OleDbDataAdapter(sqlqueryagenda, con);
            da.Fill(ds);
            return ds;
        }

        public DataSet conferences()
        {
            DataSet ds = new DataSet();
            string sqlqueryConferences = "Select * From [ConferenceEntityCollection$A1:Q7]";
            string constringConferences = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=D:\VisualStudio2013\Projects\PremierCIODataLoading\PremierCIODataLoading\Assets\ConferenceListPremierCIO.xlsx" + ";Extended Properties=\"Excel 12.0;HDR=YES;\"";
            OleDbConnection con = new OleDbConnection(constringConferences + "");
            OleDbDataAdapter da = new OleDbDataAdapter(sqlqueryConferences, con);
            da.Fill(ds);
            return ds;
        }

        public DataSet speakers()
        {
            DataSet ds = new DataSet();
            string sqlquerySpeakers = "Select * From [NewBrunswick2015$A1:P18]";
            string constringSpeakers = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=D:\VisualStudio2013\Projects\PremierCIODataLoading\PremierCIODataLoading\Assets\SpeakersNewBrunswick.xlsx" + ";Extended Properties=\"Excel 12.0;HDR=YES;\"";
            OleDbConnection con = new OleDbConnection(constringSpeakers + "");
            OleDbDataAdapter da = new OleDbDataAdapter(sqlquerySpeakers, con);
            da.Fill(ds);
            return ds;
        }
        
        public DataSet sponsors(string ConfSheet)
        {
            DataSet ds = new DataSet();
            string sponsorRange = ConfigurationSettings.AppSettings[ConfSheet];
            //TODO : Logic to fetch sponsor data from corresponding sheet.
            string sqlquerySponsors = "Select * From [" + ConfSheet + "$" + sponsorRange + "]";
            string constringSponsors = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=D:\VisualStudio2013\Projects\PremierCIODataLoading\PremierCIODataLoading\Assets\SponsorsListPremierCIO.xlsx" + ";Extended Properties=\"Excel 12.0;HDR=YES;\"";
            OleDbConnection con = new OleDbConnection(constringSponsors + "");
            OleDbDataAdapter da = new OleDbDataAdapter(sqlquerySponsors, con);
            da.Fill(ds);
            return ds;
        }
        
        public DataSet attendee()
        {
            DataSet ds = new DataSet();
            string sqlqueryAttendee = "Select * From [A1:Q318]";
            string constringAttendee = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=D:\VisualStudio2013\Projects\PremierCIODataLoading\PremierCIODataLoading\Assets\ConfirmedAttendeeListForCleveland.xlsx" + ";Extended Properties=\"Excel 12.0;HDR=YES;\"";
            OleDbConnection con = new OleDbConnection(constringAttendee + "");
            OleDbDataAdapter da = new OleDbDataAdapter(sqlqueryAttendee, con);
            da.Fill(ds);
            return ds;
        }

        
        private static AgendaItems createAgendaItemObject(DataRow dr)
        {
            AgendaItems gridagendaObj = new AgendaItems();
            gridagendaObj.itemtype = dr["itemtype"].ToString();
            //gridagendaObj.Name = dr["agendaitemname"].ToString();
            gridagendaObj.agendaitemname = dr["agendaitemname"].ToString();
            gridagendaObj.agendaheading = dr["agendaheading"].ToString();
            gridagendaObj.roomname = dr["roomname"].ToString();
            //DateTime starttime,endtime;
            if (dr["starttime"].ToString() == "0")
                gridagendaObj.starttime = 0;
            else 
            {
                DateTimeOffset resultstart = DateTimeOffset.Parse(dr["starttime"].ToString(), CultureInfo.InvariantCulture);
                //DateTime.TryParseExact(dr["starttime"].ToString(), "MM-dd-yyyy HH:mm:ss", CultureInfo.GetCultureInfo("en-US"), DateTimeStyles.None, out starttime);
                //long i = (long)starttime.ToUniversalTime().Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds;
                long stTime = EpochTimeExtensions.ToEpochTime(resultstart);
                gridagendaObj.starttime = stTime * 1000;
            }

            if (dr["endtime"].ToString() == "0")
                gridagendaObj.endtime = 0;
            else
            {
                DateTimeOffset resultend = DateTimeOffset.Parse(dr["endtime"].ToString(), CultureInfo.InvariantCulture);
                //DateTime.TryParseExact(dr["endtime"].ToString(), "MM-dd-yyyy HH:mm:ss", CultureInfo.GetCultureInfo("en-US"), DateTimeStyles.None, out endtime);
                long endTime = EpochTimeExtensions.ToEpochTime(resultend);
                gridagendaObj.endtime = endTime * 1000;
            }

            gridagendaObj.description = dr["description"].ToString();
            gridagendaObj.sidebartext = dr["sidebartext"].ToString();
            gridagendaObj.isgroup = Convert.ToBoolean(dr["isgroup"]);
            return gridagendaObj;
        }

        private static Conference createConferenceObject(DataRow dr)
        {
            Conference gridConferenceObj = new Conference();
            gridConferenceObj.Name = dr["conferencename"].ToString();
            gridConferenceObj.conferencename = dr["conferencename"].ToString();
            gridConferenceObj.city = dr["city"].ToString();
            gridConferenceObj.statecode = dr["statecode"].ToString();
            DateTime startdate, enddate;
            startdate = Convert.ToDateTime(dr["startdate"]);
            enddate = Convert.ToDateTime(dr["enddate"]);
            //DateTime.TryParseExact(dr["startdate"].ToString(), "MM-dd-yyyy HH:mm:ss", CultureInfo.GetCultureInfo("en-US"), DateTimeStyles.None, out startdate);
            //DateTime.TryParseExact(dr["enddate"].ToString(), "MM-dd-yyyy HH:mm:ss", CultureInfo.GetCultureInfo("en-US"), DateTimeStyles.None, out enddate);
            //long i = (long)starttime.ToUniversalTime().Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds;
            long stDate = EpochTimeExtensions.ToEpochDate(startdate);
            long endDate = EpochTimeExtensions.ToEpochDate(enddate);
            gridConferenceObj.startdate = stDate * 1000;
            gridConferenceObj.enddate = endDate * 1000;
            //gridConferenceObj.startdate = Convert.ToDateTime(dr["startdate"]);
            //gridConferenceObj.enddate = Convert.ToDateTime(dr["enddate"]);
            gridConferenceObj.venuename = dr["venuename"].ToString();
            gridConferenceObj.venueaddress = dr["venueaddress"].ToString();
            gridConferenceObj.venuecity = dr["venuecity"].ToString();
            gridConferenceObj.venuestatecode = dr["venuestatecode"].ToString();
            gridConferenceObj.venuepostalcode = dr["venuepostalcode"].ToString();
            gridConferenceObj.venuephone = dr["venuephone"].ToString();
            gridConferenceObj.conferenceoverview = dr["conferenceoverview"].ToString();
            gridConferenceObj.conferencestatuscode = dr["conferencestatuscode"].ToString();
            return gridConferenceObj;
        }

        private static Maps createMapObject()
        {
            Maps mapObject = new Maps();
            mapObject.mapname = "HYATT REGENCY NEW BRUNSWICK";
            mapObject.maptype = "Area";
            string imgPath = ConfigurationSettings.AppSettings["MapPath"];
            byte[] imgArray = System.IO.File.ReadAllBytes(imgPath);
            mapObject.mapimage = Convert.ToBase64String(imgArray);
            return mapObject;
        }

        private static Speakers createSpeakerObject(DataRow dr)
        {
            Speakers gridSpeakerObj = new Speakers();
            gridSpeakerObj.Name = dr["firstname"].ToString() + " " + dr["lastname"].ToString();
            gridSpeakerObj.firstname = dr["firstname"].ToString();
            gridSpeakerObj.lastname = dr["lastname"].ToString();
            gridSpeakerObj.title = dr["title"].ToString();
            gridSpeakerObj.companyname = dr["companyname"].ToString();
            gridSpeakerObj.title = dr["title"].ToString();
            string imgPath = dr["photo"].ToString();
            byte[] imgArray = System.IO.File.ReadAllBytes(imgPath);
            gridSpeakerObj.photo = Convert.ToBase64String(imgArray);
            gridSpeakerObj.description = dr["description"].ToString();
            gridSpeakerObj.contactemailaddress = dr["contactemailaddress"].ToString();
            gridSpeakerObj.contactphonenumber = dr["contactphonenumber"].ToString();
            gridSpeakerObj.websiteurl = dr["websiteurl"].ToString();
            gridSpeakerObj.linkedinprofileid = dr["linkedinprofileid"].ToString();
            gridSpeakerObj.facebookpageurl = dr["facebookpageurl"].ToString();
            gridSpeakerObj.twitterusername = dr["twitterusername"].ToString();
            gridSpeakerObj.sponsoroverview = dr["sponsoroverview"].ToString();
            return gridSpeakerObj;
        }

        private static Sponsor createSponsorObject(DataRow dr)
        {
            Sponsor gridSponsorObj = new Sponsor();
            gridSponsorObj.sponsorname = dr["sponsorname"].ToString();
            gridSponsorObj.companyname = dr["companyname"].ToString();
            gridSponsorObj.sponsorlevelcode = dr["sponsorlevelcode"].ToString();
            gridSponsorObj.contactname = dr["contactname"].ToString();
            gridSponsorObj.contactemailaddress = dr["contactemailaddress"].ToString();
            gridSponsorObj.contactphonenumber = dr["contactphonenumber"].ToString();
            gridSponsorObj.websiteurl = dr["websiteurl"].ToString();
            gridSponsorObj.linkedinprofileid = dr["linkedinprofileid"].ToString();
            gridSponsorObj.facebookpageurl = dr["facebookpageurl"].ToString();
            gridSponsorObj.twitterusername = dr["twitterusername"].ToString();
            gridSponsorObj.sponsoroverview = dr["sponsoroverview"].ToString();

            string spPath = ConfigurationSettings.AppSettings["SponsorImagePath"] + dr["logoforsite"].ToString();
            byte[] imageArray = System.IO.File.ReadAllBytes(spPath);
            gridSponsorObj.logoforsite = Convert.ToBase64String(imageArray);
            return gridSponsorObj;
        }

        private static UsergridUser createUserObject(DataRow dr)
        {
            UsergridUser gridUserObj = new UsergridUser();
            gridUserObj.UserName = dr["Email"].ToString();
            gridUserObj.Email = dr["Email"].ToString();
            gridUserObj.Name = dr["FirstName"].ToString() + " " + dr["LastName"].ToString();
            gridUserObj.usertype = dr["RegType"].ToString();
            gridUserObj.title = dr["Title"].ToString();
            gridUserObj.phonenumber = dr["Phone"].ToString();
            gridUserObj.streetaddress = dr["Address"].ToString();
            gridUserObj.city = dr["City"].ToString();
            gridUserObj.statecode = dr["State"].ToString();
            gridUserObj.postalcode = dr["ZipCode"].ToString();
            gridUserObj.country = "US";
            return gridUserObj;
        }
    }
}
