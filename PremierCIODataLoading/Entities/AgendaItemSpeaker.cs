﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Usergrid.Sdk.Model;

namespace PremierCIODataLoading.Entities
{
    class AgendaItemSpeaker : UsergridEntity
    {
        public string agendaname { get; set; }
        public string speakername { get; set; }
        public string speakerfirstname { get; set; }
        public string speakerlastname { get; set; }
        public string speakertype { get; set; }
    }
}
