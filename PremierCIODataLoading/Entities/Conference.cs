﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Usergrid.Sdk.Model;

namespace PremierCIODataLoading.Entities
{
    class Conference : UsergridEntity
    {
        public long createddate { get; set; }
        public long modifieddate { get; set; }
        public string conferencename { get; set; }
        public string city { get; set; }
        public string statecode { get; set; }
        public long startdate { get; set; }
        public long enddate { get; set; }
        public string venuename { get; set; }
        public string venueaddress { get; set; }
        public string venuecity { get; set; }
        public string venuestatecode { get; set; }
        public string venuepostalcode { get; set; }
        public string venuephone { get; set; }
        public string conferenceoverview { get; set; }
        public string conferencestatuscode { get; set; }
        public List<Sponsor> sponsors { get; set; }
        public List<UsergridUser> attendees { get; set; }
        
    }
}
