﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Usergrid.Sdk.Model;

namespace PremierCIODataLoading.Entities
{
    class Maps : UsergridEntity
    {
        public string mapname { get; set; }
        public string maptype { get; set; }
        public string mapimage { get; set; }
    }
}
