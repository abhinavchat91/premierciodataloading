﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Usergrid.Sdk.Model;

namespace PremierCIODataLoading.Entities
{
    class Speakers : UsergridEntity
    {
        public long createddate { get; set; }
        public long modifieddate { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string title { get; set; }
        public string companyname { get; set; }
        public string photo { get; set; }
        public string description { get; set; }
        public string contactemailaddress { get; set; }
        public string contactphonenumber { get; set; }
        public string websiteurl { get; set; }
        public string linkedinprofileid { get; set; }
        public string facebookpageurl { get; set; }
        public string twitterusername { get; set; }
        public string sponsoroverview { get; set; }
    }
}
