﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Usergrid.Sdk.Model;

namespace PremierCIODataLoading.Entities
{
    class User : UsergridEntity
    {
        public string username { get; set; }
        public string type { get; set; }
        public string Name { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string title { get; set; }
        public string usertype { get; set; }
        public string companyname { get; set; }
        public string streetaddress { get; set; }
        public string city { get; set; }
        public string statecode { get; set; }
        public string postalcode { get; set; }
        public string country { get; set; }
        public string emailaddress { get; set; }
        public string phonenumber { get; set; }
        public string industrytype { get; set; }
        //public string companyrevenue { get; set; }
        //public string numberofitstaff { get; set; }
        //public string annualitbudget { get; set; }
        //public string areasofinterest { get; set; }
        //public string technologysolutions { get; set; }
        //public string referral { get; set; }
        public Boolean isshareok { get; set; }


    }
}
