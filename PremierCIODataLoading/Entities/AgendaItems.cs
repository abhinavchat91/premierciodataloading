﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Usergrid.Sdk.Model;

namespace PremierCIODataLoading.Entities
{
    class AgendaItems : UsergridEntity
    {
        public string itemtype { get; set; }
        public string agendaitemname { get; set; }
        public string agendaheading { get; set; }
        public string roomname { get; set; }
        public long starttime { get; set; }
        public long endtime { get; set; }
        public string description { get; set; }
        public string sidebartext { get; set; }
        public Boolean isgroup { get; set; }
    }
}
