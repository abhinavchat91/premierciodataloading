﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Usergrid.Sdk;
using PremierCIODataLoading.Entities;
using System.IO;
using Microsoft.Office.Interop.Excel;
using System.Data.OleDb;
using System.Data;
using Usergrid.Sdk.Model;

namespace PremierCIODataLoading
{
    class GetEntities
    {
        private Client _clnt;
        
        public GetEntities(Client client)
        {
            this._clnt = client;
        }

        public UsergridCollection<Conference> getConferences()
        {
            UsergridCollection<Conference> confEntities = _clnt.GetEntities<Conference>("conferences", 1000, null);
            return confEntities;
        }

        public UsergridCollection<Sponsor> getSponsors()
        {
            UsergridCollection<Sponsor> sponsorEntities = _clnt.GetEntities<Sponsor>("sponsors", 1000, null);
            return sponsorEntities;
        }

        public UsergridCollection<UsergridUser> getUsers()
        {
            UsergridCollection<UsergridUser> userEntities = _clnt.GetEntities<UsergridUser>("users", 1000, null);
            return userEntities;
        }

        public UsergridCollection<Speakers> getSpeakers()
        {
            UsergridCollection<Speakers> speakerEntities = _clnt.GetEntities<Speakers>("speakers", 1000, null);
            return speakerEntities;
        }

        public UsergridCollection<Maps> getMaps()
        {
            UsergridCollection<Maps> mapEntities = _clnt.GetEntities<Maps>("maps", 1000, null);
            return mapEntities;
        }

        public UsergridCollection<AgendaItems> getAgendaItems()
        {
            UsergridCollection<AgendaItems> agendaitemEntities = _clnt.GetEntities<AgendaItems>("agendaItems", 1000, null);
            return agendaitemEntities;
        }

        public IList<UsergridEntity> getConnectedSponsorEntities(Connection cn)
        {
            IList<UsergridEntity> spEntities = _clnt.GetConnections(cn);
            return spEntities;
        }

        public IList<UsergridEntity> getConnectedUserEntities(Connection cn)
        {
            IList<UsergridEntity> usrEntities = _clnt.GetConnections(cn);
            return usrEntities;
        }
    }
}
