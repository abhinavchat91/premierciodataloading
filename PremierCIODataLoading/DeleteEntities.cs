﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Usergrid.Sdk;
using PremierCIODataLoading.Entities;
using System.IO;
using Microsoft.Office.Interop.Excel;
using System.Data.OleDb;
using System.Data;
using Usergrid.Sdk.Model;

namespace PremierCIODataLoading
{
    class DeleteEntities
    {
        private Client _client;
        public DeleteEntities(Client client)
        {
            this._client = client;
        }

        public bool deleteEntity(string collName,string uuid)
        {
            _client.DeleteEntity(collName, uuid);
            return true;
        }

        public void deleteAllEntities(string collName)
        {
            GetEntities gtent = new GetEntities(_client);
            switch(collName)
            {
                case "conferences":
                    Usergrid.Sdk.UsergridCollection<Conference> confrences = gtent.getConferences();
                    foreach (Conference cnf in confrences)
                    {
                        _client.DeleteEntity("conferences", cnf.Uuid);
                    }
                    break;
                case "sponsors":
                    Usergrid.Sdk.UsergridCollection<Sponsor> sponsors = gtent.getSponsors();
                    foreach (Sponsor sp in sponsors)
                    {
                        _client.DeleteEntity("sponsors", sp.Uuid);
                    }
                    break;
                case "users":
                    Usergrid.Sdk.UsergridCollection<UsergridUser> users = gtent.getUsers();
                    foreach (UsergridUser usr in users)
                    {
                        if ((usr.UserName != "ple@itrweb.com") && (usr.UserName != "test2@calance.com") && (usr.UserName != "test3@calance.com") && (usr.UserName != "testuser1@calance.com") && (usr.UserName != "testuser2@calance.com") && (usr.UserName != "testuser3@calance.com") && (usr.UserName != "ple@calance.com"))
                            _client.DeleteEntity("users", usr.Uuid);
                    }
                    break;
                case "agendaitems":
                    Usergrid.Sdk.UsergridCollection<AgendaItems> agendaitems = gtent.getAgendaItems();
                    foreach (AgendaItems ait in agendaitems)
                    {
                        _client.DeleteEntity("agendaitems", ait.Uuid);
                    }
                    break;
                case "speakers":
                    Usergrid.Sdk.UsergridCollection<Speakers> speakers = gtent.getSpeakers();
                    foreach (Speakers speaker in speakers)
                    {
                        _client.DeleteEntity("speakers", speaker.Uuid);
                    }
                    break;
                case "maps":
                    Usergrid.Sdk.UsergridCollection<Maps> maps = gtent.getMaps();
                    foreach (Maps map in maps)
                    {
                        _client.DeleteEntity("maps", map.Uuid);
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
